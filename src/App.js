import logo from './logo.svg';
import './App.css';
import { Fragment } from 'react';
import { Info } from './components/Info';
import { Form } from './components/Form';
import { ImportantInfo } from './components/ImportantInfo';
import Modal from './components/Modal';


function App() {
  return (
    <Fragment>
      <Info title="Titolo 1" content ="info 1" />
      <Info title="Titolo 2" content ="info 2" />
      <ImportantInfo title="Important Info 2" content="Informazioni importanti" />
      <Form />
    </Fragment>
  );
}

export default App;
