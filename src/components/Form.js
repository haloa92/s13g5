import { useEffect, useState } from "react";
import { Info } from "./Info";
import Modal from "./Modal";
import { Card } from "./shared/Card";
import { createPortal } from "react-dom";


export const Form = (props) => {

  const [name, setName] = useState('');
  const [isClicked, setIsClicked] = useState(false);
  const [countClick, setCountClick] = useState(0);
  const [clickedText, setClickedText] = useState('Cliccato');
  const [showModal, setShowModal] = useState(false);
  
  const onChangeHandler = (event) => {
    setName(event.target.value);
  }
  
  const onClickHandler = (event) => {
    event.preventDefault();
    setIsClicked(true);
    setCountClick((prevState) => ++prevState)
  }

  const dismissModal = () => {
    setShowModal(false);
  }

  useEffect(() => {
    const count = countClick;
    if(countClick >= 3) {
      setClickedText('Cliccato 3 volte');
      setShowModal(true);
    }
  }, [countClick]);

  useEffect(() => { // equivalente a componentDidMount
    console.log("componente renderizzato!");
  }, []);

  useEffect(() => { // equivalente a componentDidUpdate
    console.log("componente aggiornato!");
  });

  return (
    <Card className="red-bg">
      <form>
      <label>Nome</label>
        <input onChange={onChangeHandler} type="text" name="name" id="text" />
        <button onClick={onClickHandler}>Ok</button>
      </form>

      {name && <span>{name}</span>}

      {isClicked && <div>{clickedText}</div>}
      { showModal && createPortal(<Modal dismiss={dismissModal}><Info title={"Basta Cliccare"} /></Modal>, document.getElementById("modal-root"))}
    </Card>
  );
}

