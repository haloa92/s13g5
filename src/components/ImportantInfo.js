import { Card } from "./shared/Card";
import styles from './ImportantInfo.module.css';

export const ImportantInfo = (props) => {
  console.log(styles);
  return (
    <Card>
      <h1 className={styles['info-title']}>{props.title}</h1>
      <p>
        {props.content}
      </p>
    </Card>
  );
}
