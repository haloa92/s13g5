import { Fragment } from "react";
import { Card } from "./shared/Card";
import styles from "./Info.module.css";


export const Info = (props) => {
  return (
    <Card>
      <h1 className={styles["info-title"]}>{props.title}</h1>
      <p>
        {props.content}
      </p>
    </Card>
  );
}
