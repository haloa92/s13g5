const Modal = (props) => {

  return (
    <div className="overlay">
      <div className="modal-container">
        <div className="modal">
          <h2 className="modal-title">Modale</h2>
          <div>{props.children}</div>
          <div>
            <button onClick={props.dismiss}>Ok</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;